import React from 'react';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      todoList: [],
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleInputChange(event) {
    this.setState({
      text: event.target.value,
    })
  }

  handleFormSubmit(event) {
    event.preventDefault();

    const { text, todoList } = this.state;
    const newTodoList = [...todoList, text];

    this.setState({
      todoList: newTodoList,
      text: '',
    });
  }

  handleDelete() {
    console.log('TODO: delete this item');
  }

  render() {
    const { text, todoList } = this.state;

    return (
      <div className="container mt-3">
        <form onSubmit={this.handleFormSubmit} className="mb-3">
          <label htmlFor="todo" className="form-label">
            Todo title
          </label>
          <input
            id="todo"
            type="text"
            className="form-control"
            placeholder="Add todo..."
            value={text}
            onChange={this.handleInputChange}
          />
        </form>

        <ul className="list-group">
          {
            todoList.map(todoItem => (
              <li
                key={todoItem}
                onClick={this.handleDelete}
                className="list-group-item"
              >
                {todoItem}
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

export default App;
